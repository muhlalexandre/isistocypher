package principal;

public class Association {

	private Relation relation12;
	private Relation relation21;
	
	public Association(Relation relation12, Relation relation21){
		this.relation12 = relation12;
		this.relation21 = relation21;
	}
	
	public String afficheMatch(){
		return relation12.afficheMatch()+"\n"+ relation21.afficheMatch();
	}
	public String afficheCreate(){
		return relation12.afficheCreate()+"\n"+ relation21.afficheCreate();
	}
	
	
}
