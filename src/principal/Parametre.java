package principal;

public class Parametre {
	private String name;
	private String multipliciteMin;
	private String multipliciteMax;
	
	public Parametre(String name, String multipliciteMin, String multipliciteMax){
		this.name = name;
		this.multipliciteMin = multipliciteMin;
		this.multipliciteMax = multipliciteMax;
	}
	
	public String affiche(){
		return ","+name+":'("+multipliciteMin+".."+multipliciteMax+")'";
	}
}
