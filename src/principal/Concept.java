package principal;

import java.util.ArrayList;

public class Concept {
	public static enum Categorie{
		primaire,
		secondaire
	}
	
	private String nom;
	private int isisID;
	private String nomcomplet;
	private String description;
	private String datemaj;
	private Categorie categorie;
	private ArrayList<Parametre> parametres;
	private ArrayList<Relation> relations;
	private boolean created;
	
	public Concept(int isisID,String nom,String nomcomplet,String description,String datemaj,Categorie categorie) {
		this.isisID = isisID;
		this.nom = nom;
		this.nomcomplet = nomcomplet;
		this.description = description;
		this.datemaj = datemaj;
		this.categorie = categorie;
		this.parametres = new ArrayList<Parametre>();
		this.relations = new ArrayList<Relation>();
		this.created = false;
	}
	
	public void addParametre(Parametre parametre){
		parametres.add(parametre);
	}
	
	public boolean isCreated(){
		return created;
	}

	public String getNom() {
		return nom;
	}

	public int getIsisID() {
		return isisID;
	}

	public String getNomcomplet() {
		return nomcomplet;
	}

	public String getDescription() {
		return description;
	}

	public String getDatemaj() {
		return datemaj;
	}

	public Categorie getCategorie() {
		return categorie;
	}
	
	public void addRelation(Relation relation){
		this.relations.add(relation);
	}
	
	public String getCreate(){
		String create = "CREATE ("+getNom()+":"+getNom()+"{id:"+getIsisID()+",nom:'"+getNom()+"',nomcomplet:'"+getNomcomplet()+"',datemaj:'"+getDatemaj()+"',description:'"+getDescription()+"'";
		for (Parametre parametre : parametres) {
			create += parametre.affiche();
		}
		for (Relation relation : relations) {
			create += relation.affiche();
		}
		create += "})";
		this.created = true;
		return create;
	}
	
	
	
}
