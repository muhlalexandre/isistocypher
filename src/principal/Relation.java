package principal;

import principal.Concept.Categorie;

public class Relation{
	private String nom;
	private Concept domaine;
	private Concept codomaine;
	private String multiplicitemin;
	private String multiplicitemax;
	
	public Relation(String nom,Concept from,Concept to,String min, String max) {
		this.nom = nom;
		this.domaine = from;
		this.codomaine = to;
		this.multiplicitemin = min;
		this.multiplicitemax = max;
		if(this.domaine.getCategorie() == Categorie.secondaire && this.codomaine.getCategorie() == Categorie.secondaire)
			this.domaine.addRelation(this);
	}

	public String afficheMatch() {
		return "MATCH ("+domaine.getNom()+"),("+codomaine.getNom()+")";
	}
	
	public String afficheCreate(){
		return "CREATE ("+domaine.getNom()+")-[:"+nom+"{multiplicité_min:'"+multiplicitemin+"', multiplicité_max:'"+multiplicitemax+"'}]->("+codomaine.getNom()+")";
	}
	
	public String affiche(){
		return ","+nom+":'("+multiplicitemin+".."+multiplicitemax+")'";
	}
	
	public Concept getDomaine(){
		return domaine;
	}
	
	public Concept getCodomaine(){
		return codomaine;
	}
	
	public String getMultipliciteMin(){
		return multiplicitemin;
	}
	
	public String getMultipliciteMax(){
		return multiplicitemax;
	}
}