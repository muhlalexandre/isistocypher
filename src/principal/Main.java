package principal;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import principal.Concept.Categorie;

public class Main {

	private static File xmlFile;
	private static Document xmlDocument;
	private static HashMap<Integer, Concept> concepts;
	private static ArrayList<Association> associations;
	
	public static void main(String[] args) {
		
		concepts = new HashMap<Integer, Concept>();
		associations = new ArrayList<Association>();
		
		if(args.length < 1){
			System.out.println("Veuillez indiquer le fichier xml comme premier argument");
			System.exit(2);
		}else{
			xmlFile = new File(args[0]);
			try{
				new FileInputStream(xmlFile);
			}catch(FileNotFoundException e){
				System.out.println("Fichier non trouv�");
				System.exit(1);
			}
		}
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			xmlDocument = builder.parse(xmlFile);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(xmlDocument.getDocumentElement().getTagName() != "isis"){
			System.out.println("Ce fichier XML ne semble pas �tre un fichier XML Isis, voulez vous continuer quand m�me ? (O/N)");
			Scanner reader = new Scanner(System.in);
			String choix = reader.nextLine();
			if(choix.equalsIgnoreCase("N")){
				System.exit(0);
			}
		}
		
		if(xmlDocument.getDocumentElement().getElementsByTagName("lesConcepts") != null){
			concepts(xmlDocument.getDocumentElement().getElementsByTagName("concept"));
		}
		
		if(xmlDocument.getDocumentElement().getElementsByTagName("lesAssociations") != null){
			associations(xmlDocument.getDocumentElement().getElementsByTagName("association"));
		}
		String filename = "";
		if(args.length < 2){
			filename  = args[0].substring(0, args[0].length()-3);
			filename += "cypher";
			System.out.println("Le nom du fichier de sortie par defaut sera "+filename+"\nVoulez vous le modifier ? (O/N)");
			Scanner reader = new Scanner(System.in);
			String choix = reader.nextLine();
			if(choix.equalsIgnoreCase("O")){
				System.out.println("Entrez le nom du ficher : ");
				filename = reader.nextLine();
			}
		}else{
			filename = args[1];
		}
		
		
		
		try {
			PrintWriter writer = new PrintWriter(filename, "UTF-8");
			
			for (Concept concept : concepts.values()) {
				writer.println(concept.getCreate());
			}
			String with = "WITH ";
			for (Concept concept : concepts.values()) {
				with+=concept.getNom()+",";
			}
			with = with.substring(0,with.length()-1);
			writer.println(with);
			for (Association assoc : associations) {
				writer.println(assoc.afficheMatch());
			}
			for (Association assoc : associations) {
				writer.println(assoc.afficheCreate());
			}
			
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private static void concepts(NodeList list){
	
		Node node,child;
		for (int i = 0; i < list.getLength(); i++) {
			String nom = "",nomcomplet = "",description = "",dateMaj = "";
			Categorie categorie = null;
			node = list.item(i);
			dateMaj += node.getAttributes().getNamedItem("dateMaj").getTextContent();
			int id = Integer.parseInt(node.getAttributes().getNamedItem("id").getTextContent());
			for (int j = 0; j < node.getChildNodes().getLength(); j++) {
				child = node.getChildNodes().item(j);
				switch (child.getNodeName()) {
					case "nom":
						nom += child.getTextContent();
						break;
					case "nomComplet":
						nomcomplet += child.getTextContent();
						break;
					case "description":
						description += child.getTextContent();
						break;
					case "categorie":
						if(child.getTextContent().equals("primaire")){
							categorie = Categorie.primaire;
						}else if(child.getTextContent().equals("secondaire")){
							categorie = Categorie.secondaire;
						}
						break;
					default:
						break;
				}
			}
			concepts.put(id, new Concept(id, nom, nomcomplet, description, dateMaj, categorie));
		}
		
	}
	
	private static void associations(NodeList list){
		Node node,child,child2;
		for(int i = 0;i < list.getLength();i++){
			String nom = "";
			int ens1 = 0, ens2 = 0;
			Relation relation1 = null,relation2 = null;
			node = list.item(i);
			for(int j = 0;j < node.getChildNodes().getLength();j++){
				child = node.getChildNodes().item(j);
				switch(child.getNodeName()){
					case "ens1":
						ens1 = Integer.parseInt(child.getTextContent());
						break;
					case "ens2":
						ens2 = Integer.parseInt(child.getTextContent());
						break;
					case "relation":
						String relnom = "", max = "", min = "";
						int from = 0, to = 0;
						for(int k = 0;k < child.getChildNodes().getLength();k++){
							child2 = child.getChildNodes().item(k);
							switch (child2.getNodeName()) {
								case "nom":
									relnom += child2.getTextContent();
									break;
								case "domaine":
									from = Integer.parseInt(child2.getTextContent());
									break;
								case "codomaine":
									to = Integer.parseInt(child2.getTextContent());
									break;
								case "multipliciteMax":
									max += child2.getTextContent();
									break;
								case "multipliciteMin":
									min += child2.getTextContent();
									break;
							}
						}
						if(from == ens1){
							relation1 = new Relation(relnom, concepts.get(from), concepts.get(to), min, max);
						}else{
							relation2 = new Relation(relnom, concepts.get(from), concepts.get(to), min, max);
						}
						break;
					default:
						break;
				}
			}
			if(relation1.getDomaine().getCategorie().equals(Categorie.secondaire) && relation2.getDomaine().getCategorie().equals(Categorie.secondaire)){
				associations.add(new Association(relation1, relation2));
			}else if(relation1.getDomaine().getCategorie().equals(Categorie.primaire)){
				relation1.getCodomaine().addParametre(new Parametre(relation1.getDomaine().getNom(), relation1.getMultipliciteMin(), relation1.getMultipliciteMax()));
				concepts.remove(relation1.getDomaine().getIsisID());	
			}else if(relation2.getDomaine().getCategorie().equals(Categorie.primaire)){
				relation2.getCodomaine().addParametre(new Parametre(relation2.getDomaine().getNom(), relation2.getMultipliciteMin(), relation2.getMultipliciteMax()));
				concepts.remove(relation2.getDomaine().getIsisID());
			}
		}
	}

}
